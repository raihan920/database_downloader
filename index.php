<?php
$hostName = "localhost";
$userName = "root";
$password = "";
$database = "test_db";

date_default_timezone_set('Asia/Dhaka');

$date = date('d-m-Y-h_i_s_A');
var_dump($date);

include_once(dirname(__FILE__) . '/mysqldump-php-master/src/Ifsnop/Mysqldump/Mysqldump.php');
$dump = new Ifsnop\Mysqldump\Mysqldump("mysql:host=$hostName;dbname=$database", "$userName", "$password");
$backupDir = "storage/backups/backup-{$database}-{$date}.sql";
$dump->start($backupDir);